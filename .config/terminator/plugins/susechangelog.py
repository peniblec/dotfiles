'''Terminator plugin to make bug references clickable in RPM changelogs.'''

import re
from terminatorlib.plugin import URLHandler


AVAILABLE = ['SuseTrackersURLHandler']


_TRACKERS = {
    'bgo': 'https://bugzilla.gnome.org/show_bug.cgi?id={}',
    'bmo': 'https://bugzilla.mozilla.org/show_bug.cgi?id={}',
    'bnc': 'https://bugzilla.novell.com/show_bug.cgi?id={}',
    'boo': 'https://bugzilla.opensuse.org/show_bug.cgi?id={}',
    'bsc': 'https://bugzilla.suse.com/show_bug.cgi?id={}',
    'fate': 'https://features.opensuse.org/{}',
    'fdo': 'https://bugs.freedesktop.org/show_bug.cgi?id={}',
    'glgo#gnome/gtk': 'https://gitlab.gnome.org/GNOME/gtk/issues/{}',
    'kde': 'https://bugs.kde.org/show_bug.cgi?id={}'
}

_PATTERN = r'(?i)(?P<tracker>{t})#(?P<id>{i})'.format(
    t='|'.join(_TRACKERS),
    i='[0-9]+(?:#c[0-9]+)?'
)

_REGEX = re.compile(_PATTERN)


class SuseTrackersURLHandler(URLHandler):
    capabilities = ['url_handler']
    handler_name = 'suse_rpm_trackers'
    match = r'\b'+_PATTERN+r'\b'

    def callback(self, ref):
        fields = _REGEX.match(ref).groupdict()
        template = _TRACKERS[fields['tracker'].lower()]
        bug_id = fields['id']
        return template.format(bug_id)
