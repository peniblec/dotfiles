;; -*- lexical-binding: t -*-
;; user-full-name: from /etc/passwd, chfn(1).
;; user-mail-address: from EMAIL variable, ~/.profile & ~/.xsessionrc.

;; In ~/.authinfo.gpg:
;;      machine imap.gmail.com login LOGIN password PASSWORD port 993
;;      machine smtp.gmail.com login LOGIN password PASSWORD port 587
;;      machine irc.freenode.net login LOGIN password PASSWORD

(setq gnus-select-method
      '(nnimap "gmail"
               (nnimap-address "imap.gmail.com")
               (nnimap-server-port 993)
               (nnmail-expiry-target "nnimap+gmail:[Gmail]/Trash")
               (nnmail-expiry-wait immediate))
      gnus-secondary-select-methods
      '((nntp "archive.lwn.net")
        (nntp "news.gmane.io"))
      gnus-summary-line-format "%*%U%R  %-16,16&user-date; %B%-23,23f  %s\n"
      gnus-summary-dummy-line-format "                     ╭                          %S\n"
      gnus-summary-make-false-root 'dummy
      gnus-sum-thread-tree-root "╭ "
      gnus-sum-thread-tree-false-root "┬ "
      gnus-sum-thread-tree-single-indent "  "
      gnus-sum-thread-tree-indent " "
      gnus-sum-thread-tree-single-leaf "╰► "
      gnus-sum-thread-tree-leaf-with-other "├► "
      gnus-sum-thread-tree-vertical "│"
      gnus-thread-sort-functions
      '(gnus-thread-sort-by-number
        (not gnus-thread-sort-by-most-recent-date))
      gnus-treat-display-smileys nil
      gnus-user-date-format-alist '(((gnus-seconds-today)
                                     . "%H:%M")
                                    ((+ 86400 (gnus-seconds-today))
                                     . "Yesterday %H:%M")
                                    ((* 6 86400)
                                     . "%a %H:%M")
                                    ((gnus-seconds-month)
                                     . "%a %d")
                                    ((gnus-seconds-year)
                                     . "%b %d")
                                    (t
                                     . "%F"))
      gnus-gcc-mark-as-read t)

;; message-subject-re-regexp is used both in Gnus summary buffers to
;; detect and elide similar subjects in a thread, and by message mode
;; when replying, to determine what to strip from the subject.
;;
;; Some MUAs add cruft to the subject, turning "Re: bug#123: foobar"
;; into "RE: [External] : Re: bug#1234: foobar", which Debbugs will
;; then turn into "bug#1234: [External] : Re: bug#1234: foobar".
;;
;; The only way I can find to tell the Gnus summary code to
;; canonicalize all that cruft away is by tweaking this regexp, but
;; setting its global value causes message mode to elide stuff it
;; shouldn't.  Therefore, chase down the best Gnus hook for the job,
;; and set the regexp locally.
(defun my/gnus-reply-prefixes ()
  (mapcan (lambda (prefix) (list prefix (upcase prefix) (capitalize prefix)))
          '("re" "aw" "sv" "fw" "fwd")))

(setq my/gnus-summary-normalize-subject
      (rx-to-string
       `(seq bol
             (+ (or ,@(my/gnus-reply-prefixes)
                    (seq "bug#" (+ digit))
                    (seq "[" (or "External" "SPAM UNSURE") "]"))
                (? (* space) ":") (* space)))))

(add-hook 'gnus-summary-generate-hook
          (lambda ()
            (setq-local message-subject-re-regexp
                        my/gnus-summary-normalize-subject)))

(gnus-add-configuration
 '(summary
   (horizontal 1.0
               (vertical 40 (group 1.0))
               (vertical 1.0 (summary 1.0 point)))))
(gnus-add-configuration
 '(article
   (horizontal 1.0
               (vertical 40 (group 1.0))
               (vertical 1.0
                         (summary 0.25 point)
                         (article 1.0)))))

(let* ((initials (mapconcat (lambda (s) (substring s 0 1))
                            (split-string user-full-name)
                            nil))
       (sent-prefix (format "%s → " initials)))
  (setq gnus-summary-to-prefix sent-prefix
        gnus-summary-newsgroup-prefix sent-prefix))

(setq smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587)

;; Key bindings:
;;
;; When browsing group buffer:
;;
;; L            list all groups
;; RET          view unread mail in group
;; C-u RET      view all mail in group
;; g            refresh
;; G G          search group
;;
;; When browsing summary buffer:
;;
;; B m          move message to group
;; / N          fetch new
;; M-g          refresh (expire, move, fetch new, show unread)
;; C-u M-g      refresh (expire, move, fetch new, show all)
;; C-u g        show raw, undecoded message source; g to decode
;; T h          collapse (hide) thread
;; T s          expand (show) thread
;; T k, C-M-k   mark thread as read
;; M-1 T k      mark thread as unread
;; r            reply
;; R            reply (quoting)
;; S w          reply-all
;; S W          reply-all (quoting)
;; C-c C-f      forward
;; d            mark read
;; M-u          clear marks (≡ mark unread)
;; E            expire
;; #            mark for next action
;; M-#, M P u   unmark for next action
;;
;;      In draft summary buffer:
;;
;;      D e     edit draft
;;
;; Both:
;;
;; m            compose
;;
;; When reading article:
;;
;; o            save attachment at point
;; K b          add button for inlined MIME part
;;
;; When composing:
;; C-c C-c      send
;; C-c C-a      attach
;; C-c C-f s    change the subject (append "was:")
;;
;; FAQ:
;;
;; - how to see *all mails*, not just unread?
;;      - C-u RET
;;
;; - how to delete mail?
;;      - E to mark as expired
;;      - C-u M-g to refresh
;;
;; - how to remove groups deleted on the IMAP server?
;;      - b to iterate over "bogus" groups and remove them
;;
;; - how to list most-recent mails on top?
;;      - cf. gnus-thread-sort-functions
;;
;; - how to close a mail without going back to the group list?
;;      - = to make summary full-screen
;;
;; - how to get contact completion?
;;      - install ebdb from GNU ELPA
;;      - or just use message-mail-alias-type
;;
;; - how to refresh?
;;      - summary buffer:
;;          - / N (fetch new)
;;          - M-g (expire, move, fetch & redisplay)
;;      - group buffer: g
;;
;; - what do all those letters mean?
;;   (info "(gnus) Marking Articles")
;;      - O     old ≡ read during previous session
;;      - R     just read
;;      - r     manually marked as read
;;      - A     answered
;;      - E     expirable
;;      - G     cancelled (e.g. moved somewhere else)
;;      - .     unseen
;;
;; - how to subscribe to mailing lists?
;;      - to browse an NNTP server, either
;;          - hit B in the group buffer, then nntp *some server*
;;          - or add (nntp "*some server*") to gnus-secondary-methods
;;      - over the list: u
;;
;; TODOs:
;;
;; - gnus-summary-line-format (📎 for attachments)
;;
;; - how to archive mails and news locally?
;;
;; - describe-key is mostly useless in article mode:
;;   > X runs the command gnus-article-read-summary-keys
;;
;; - detect possibly missing attachments from keywords
